吳存富
主持律師

輔仁大學法律系司法組畢業
國立政治大學法律研究所(在職專班就讀中)
齊麟國際法律事務所法律研究員
博仲法律事務所法律研究員
國家考試律師高考及格
台英國際法律事務所律師
可道律師事務所主持律師
中華不動產仲裁協會仲裁人
中華民國仲裁協會仲裁人

----------------
李尚宇
顧問律師

屏東地檢署檢察官
台南地檢署檢察官
台北地檢署檢察官
國家考試律師高考及格
專利代理人
東吳大學法學碩士
國立政治大學傳播學院碩士班
國立中正大學戰略暨國際關係碩士
國立台北大學法學士
文化大學推廣教育中心法律講座講師

----------------
黃韋儒
合夥律師

世新大學法律學系畢業
可道律師事務所法務專員
國家考試律師高考及格
可道律師事務所受僱律師
可道律師事務所合夥律師

----------------
溫俊國
合夥律師

世新大學法律系畢業
臺灣高等法院高雄分院法官助理
臺灣高雄少年及家事法院訴訟輔導科專員
國家考試律師高考及格
可道律師事務所合夥律師

----------------
陳庭琪
合夥律師

台大法學碩士
台大法律系財經法學組學士
國家考試律師高考及格
可道律師事務所合夥律師

----------------
林修平
合夥律師

東海大學法律系畢業
世新大學法學組碩士
國家考試律師高考及格
可道律師事務所受僱律師

----------------
鐘依庭
合夥律師

國立中正大學財經法律學系學士
國立中央大學法律與政府研究所碩士
中央選舉委員會科員
永平國際法律事務所受雇律師
台北市危老推動師
可道律師事務所受雇律師
可道律師事務所合夥律師

----------------
張立民
合夥律師

東吳大學法律系
東吳大學法律系法律研究所民法組
可道律師事務所受雇律師
可道律師事務所合夥律師

----------------
楊喬閔
合夥律師

國立成功大學法律系研究所科際整合法律組碩士
國家考試律師高考及格
可道律師事務所合夥律師

----------------
梁惟翔
合夥律師

國立政治大學法律系學士
國立政治大學刑事法組碩士班
國家考試律師高考及格
可道律師事務所合夥律師
志光公職刑事法講師

----------------
徐亦安
合夥律師

東吳大學法律學系學士
國家考試律師高考及格
可道律師事務所受雇律師
衡律法律事務所受雇律師
可道律師事務所合夥律師

----------------
彭志煊
合夥律師

國立政治大學法律系財經法組
萊爾富國際股份有限公司法務

----------------
許亞哲
合夥律師

國立台北大學法律系學士
國立政治大學財經法組碩士班
國家考試律師高考及格
中國法律職業資格考試合格
可道律師事務所受雇律師
可道律師事務所合夥律師

----------------
李宜真
律師

中國文化大學法律系畢業
國家考試律師高考及格
可道律師事務所受雇律師

----------------
郭光煌
律師

----------------
張媁婷
律師

國立中興大學法律學系暨歷史學系學士
滙利國際商務法律事務所法務助理
國家考試律師高考及格
眾律國際法律事務所受僱律師
可道律師事務所受僱律師

----------------
林品君
律師

國立臺北大學法律學系司法組學士
國家考試律師高考及格
可道律師事務所實習律師
可道律師事務所受僱律師

----------------





全台灣服務
可道全台服務，不論您有什麼法律問題，我們都能為您服務!

專業法律團隊
知名吳存富律師領銜法律顧問團隊，是您最堅強的後盾!

線上即時諮詢
您有法律問題嗎?我們有提供Line線上專業的律師團隊協助您!

各業界指名推薦
企業及民眾優先指名推薦最專業服務最好的法律顧問!