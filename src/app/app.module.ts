import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { TeamComponent } from './team/team.component';
import { ContentComponent } from './content/content.component';
import { AddressComponent } from './address/address.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ShareComponent } from './share/share.component';
import { NewsComponent } from './news/news.component';
import { AllteamComponent } from './allteam/allteam.component';
import { AdviserComponent } from './adviser/adviser.component';
import { ReserveComponent } from './reserve/reserve.component';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { ContactComponent } from './contact/contact.component';
import { PropertyComponent } from './property/property.component';
import { CriminalComponent } from './criminal/criminal.component';
import { RealEstateComponent } from './real-estate/real-estate.component';
import { MarriageComponent } from './marriage/marriage.component';
import { FamilyComponent } from './family/family.component';
import { LaborComponent } from './labor/labor.component';
import { DebtComponent } from './debt/debt.component';
import { ExhortComponent } from './exhort/exhort.component';
import { ApartmentComponent } from './apartment/apartment.component';
import { IvyCarouselModule } from 'angular14-responsive-carousel';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    TeamComponent,
    ContentComponent,
    AddressComponent,
    FooterComponent,
    HomeComponent,
    ShareComponent,
    NewsComponent,
    AllteamComponent,
    AdviserComponent,
    ReserveComponent,
    QrcodeComponent,
    ContactComponent,
    PropertyComponent,
    CriminalComponent,
    RealEstateComponent,
    MarriageComponent,
    FamilyComponent,
    LaborComponent,
    DebtComponent,
    ExhortComponent,
    ApartmentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IvyCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
