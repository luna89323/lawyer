import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExhortComponent } from './exhort.component';

describe('ExhortComponent', () => {
  let component: ExhortComponent;
  let fixture: ComponentFixture<ExhortComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExhortComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExhortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
