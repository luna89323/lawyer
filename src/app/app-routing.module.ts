import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddressComponent } from './address/address.component';
import { AdviserComponent } from './adviser/adviser.component';
import { AllteamComponent } from './allteam/allteam.component';
import { ApartmentComponent } from './apartment/apartment.component';
import { ContactComponent } from './contact/contact.component';
import { CriminalComponent } from './criminal/criminal.component';
import { DebtComponent } from './debt/debt.component';
import { ExhortComponent } from './exhort/exhort.component';
import { FamilyComponent } from './family/family.component';
import { HomeComponent } from './home/home.component';
import { LaborComponent } from './labor/labor.component';
import { MarriageComponent } from './marriage/marriage.component';
import { NewsComponent } from './news/news.component';
import { PropertyComponent } from './property/property.component';
import { QrcodeComponent } from './qrcode/qrcode.component';
import { RealEstateComponent } from './real-estate/real-estate.component';
import { ReserveComponent } from './reserve/reserve.component';
import { ShareComponent } from './share/share.component';

const routes: Routes = [
  {path:"",redirectTo:"home",pathMatch:"full"},
  {path:"home",component:HomeComponent},
  {path:"share",component:ShareComponent},
  {path:"news",component:NewsComponent},
  {path:"allteam",component:AllteamComponent},
  {path:"adviser",component:AdviserComponent},
  {path:"reserve",component:ReserveComponent},
  {path:"qrcode",component:QrcodeComponent},
  {path:"contact",component:ContactComponent},
  {path:"property",component:PropertyComponent},//智慧財產
  {path:"criminal",component:CriminalComponent},//刑事案例
  {path:"real-estate",component:RealEstateComponent},//不動產
  {path:"marriage",component:MarriageComponent},//婚姻親子
  {path:"family",component:FamilyComponent},//家事繼承
  {path:"labor",component:LaborComponent},//勞資糾紛
  {path:"debt",component:DebtComponent},//債務
  {path:"exhort",component:ExhortComponent},//小叮嚀
  {path:"apartment",component:ApartmentComponent},//公寓大廈
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
